---?color=linear-gradient(to left, #176CEB, #452EE1)

![](img/computerroom.jpg)
@snap[north span-100 headline text-white]
# @color[white]( Running your own mainframe on Linux (for fun and profit) )
@snapend

---?color=linear-gradient(to left, #176CEB, #452EE1)

@snap[west span-65]
# @color[white](Who am I?)
@ul[text-white](false)
- Jeroen Baten 
- Lifetime Innovator
- Job title : IT Solutionist
- Author of 8 books
- Dad of 5 girls 
@ulend
@snapend

@snap[east span-40]
@ul[](false)
![](img/lihb.png)
![](img/linuxmag.png)
![](img/lpi2.png)
![](img/osbbeo.png)
@ulend
@snapend

---?color=linear-gradient(to left, #176CEB, #452EE1)

[//]@snap[north headline text-white]
### @color[white](Who am I ? (in pics) )
[//]@snapend

@snap[south-west span-40]
![](img/hond.png)
![](img/bus2.png)
@snapend

@snap[east span-40]
![](img/rk.png)

![](img/helm.png)
@snapend

---?color=linear-gradient(to left, #176CEB, #452EE1)


@snap[north headline text-white]
### @color[white](My main project)
@snapend

@snap[west span-25]
![](img/lp-logo2.png?size=40% auto)
@ul[text-white](false)
- Web-based project management appl.
- Very cool!
@ulend
@snapend

@snap[east span-70]

![](img/lp.png)
@snapend


---?color=linear-gradient(to left, #176CEB, #452EE1)

@snap[north span-100 headline text-white]
## @color[white](Let me start with apologies first...)
@snapend

@snap[west span-60]
@color[white](Talking the same language… can still cause a culture clash.  With respect to the code of conduct: Please forgive me where applicable.  (Trick: Dutch people take everything literally!) )
@snapend

@snap[east span-40]
![](img/english.gif)
@snapend



---?color=linear-gradient(to left, #176CEB, #452EE1)
@snap[north span-100 headline text-white]
### @color[white](What is a mainframe?)
@snapend

@snap[west span-25]
@color[white](Massive hardware parallelisation)
@snapend

@snap[east span-70]
![](img/mainframe.gif)
@snapend

---?color=linear-gradient(to left, #176CEB, #452EE1)
@snap[north span-100 headline text-white]
### @color[white](What is Hercules?)
@snapend

@snap[]
@ul[text-white](false)
- A IBM Mainframe emulator for:
- System/370 
- ESA/390 
- 64-bit z/Architecture. 
- Hercules runs under Linux, Windows, Solaris, FreeBSD, and Mac OS X
@ulend
@snapend









